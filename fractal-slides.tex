\documentclass[xcolor=pdftex,dvipsnames,table]{presentation}
\usetheme{Warsaw}
\usecolortheme[RGB={0,40,100}]{structure}
\useinnertheme{rounded}

\usepackage{pgf,tikz}
\usepackage{mathrsfs}
\usetikzlibrary{arrows}

\definecolor{cqcqcq}{rgb}{0.3,0.1,0.1}

\def\card{\operatorname{card}}
\def\diam{\operatorname{diam}}
\def\dim{\operatorname{dim}}

\begin{document}

\title[Middle-$\lambda$ Cantor Set] % (optional, only for long titles)
{
	Box Counting Dimension of the Middle-$\lambda$ Cantor Set
}
%\subtitle{Text Here}
\author[McCarthy] % (optional, for multiple authors)
{
	Matt McCarthy
}
\institute[Institution] % (optional)
{
	Christopher Newport University
}
\date[date] % (optional)
{
	CNU Paideia 2016\\
	April 16, 2016
}

%\subject{subject}

\frame{\titlepage}

\section{The Cantor Set}

\begin{frame}
	\begin{enumerate}
		\item Suppose $x\in[0,1]$
		\item Then
		\[
			x = \sum_{i=0}^\infty x_i 3^{-i}
		\]
		where $x_i\in\set{0,1,2}$ for all $i$ and $x_0=0$ or $x_0=1$.
		\item This is the \textit{ternary expansion of $x$}
	\end{enumerate}
	\pause
	\vspace{1em}
	\begin{center}
		\begin{Large}
			Consider the set of numbers in $[0,1]$ that have no 1's in their ternary expansion.
		\end{Large}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Constructing the Middle-third Cantor Set}

	\begin{center}
		\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=0.3cm,y=1.0cm]
		\clip(-2,-6.) rectangle (28.,0.);
		\draw [line width=.5mm] (0.,-1.)-- (27.,-1.);
		\draw [line width=.5mm] (-2.2,-0.75) node[anchor=north west] {$C_0$};
		\draw [line width=.5mm] (-.5,-1.25) node[anchor=north west] {$0$};
		\draw [line width=.5mm] (26,-1.25) node[anchor=north west] {$1$};
		
		\pause
		\draw [line width=.5mm,color=magenta] (0.,-3.)-- (27.,-3.);
		\draw [line width=.5mm] (0.,-3.)-- (9.,-3.);
		\draw [line width=.5mm] (27.,-3.)-- (18.,-3.);
		\draw [line width=.5mm] (-2.2,-2.75) node[anchor=north west] {$C_1$};
		\draw [line width=.5mm] (-.5,-3.25) node[anchor=north west] {$0$};
		\draw [line width=.5mm] (26,-3.25) node[anchor=north west] {$1$};
		\draw [line width=.5mm] (7.5,-3.25) node[anchor=north west] {$1/3$};

		\pause
		\draw [line width=.5mm, color=magenta] (0.,-5.)-- (27.,-5.);
		\draw [line width=.5mm] (0.,-5.)-- (3.,-5.);
		\draw [line width=.5mm] (6.,-5.)-- (9.,-5.);
		\draw [line width=.5mm] (18.,-5.)-- (21.,-5.);
		\draw [line width=.5mm] (24.,-5.)-- (27.,-5.);
		\draw [line width=.5mm] (-2.2,-4.75) node[anchor=north west] {$C_2$};
		\draw [line width=.5mm] (-.5,-5.25) node[anchor=north west] {$0$};
		\draw [line width=.5mm] (26,-5.25) node[anchor=north west] {$1$};
		\draw [line width=.5mm] (1.5,-5.25) node[anchor=north west] {$1/9$};
		\end{tikzpicture}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Properties of the Cantor Set}

	\begin{enumerate}
		\item Uncountable
		\item Has no length
		\item Basis of many counterexamples in mathematics
		\item Consists of numbers in $[0,1]$ whose base 3 expansion have no 1s
		\pause
		\item A fractal
	\end{enumerate}
\end{frame}

\section{Box Counting Dimension}

\begin{frame}
	\frametitle{Why Study Fractals?}

	What is a fractal?
	\begin{enumerate}
		\item Self-similar
		\item Has detail at arbitrarily small scales
		\item Constructed recursively
		\item Has a difficult geometry
		\item Large and small simultaneously
	\end{enumerate}
	\pause
	Why fractals?
	\begin{enumerate}
		\item Model natural phenomena that standard geometries cannot
	\end{enumerate}
\end{frame}

\begin{frame}
	\frametitle{Dimension}

	How do we define fractals?

	\pause

	\begin{center}
		There is no formal definition of fractal!
	\end{center}

	\pause

	Can we find the ``size'' of sets like the Cantor set?
	\pause
	\vspace{1em}

	Consider a vector space $V$ over $\RR$.
	\begin{enumerate}
		\item $V$ has a \textit{dimension} over $\RR$.
	\end{enumerate}

	\pause

	\begin{center}
		We \textit{extend} our notion of dimension.
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Box-Counting Dimension}

	\begin{definition}
		Let $X\subset\RR^n$ be nonempty and $\delta>0$.
		Then a \textit{$\delta$-cover} of $X$ is a set $D=\set{D_i}_{i=1}^{n_D}$ such that $X\subseteq \cup_{D_i\in D} D_i$ and $\diam D_i \leq \delta$ for all $D_i\in D$.
		We denote the collection of all such covers as $\mathcal{D}_\delta(X)$.
	\end{definition}
	\begin{definition}
		Let $X$ be a nonempty subset of $\RR^n$.
		Then the \textit{box-counting dimension} of $X$, denoted $\dim_B(X)$, is defined as
		\[
			\lim\limits_{\delta\rightarrow0} \frac{\ln N_\delta(X)}{-\ln\delta}
		\]
		where
		\[
			N_\delta(X) = \min\limits_{D\in\mathcal{D}_\delta(X)} \card{D}.
		\]
	\end{definition}
\end{frame}

\begin{frame}
	\frametitle{Removing the Middle-$\lambda$ from $[0,a]$}

	Suppose $a>0$.

	\begin{enumerate}
		\item Remove exactly one interval of length $a\lambda$ from $[0,a]$
		\begin{enumerate}
			\item Want exactly two disjoint closed intervals of equal length left over
		\end{enumerate}
		\pause
		\item We removed $a\lambda$ from the interval
		\begin{enumerate}
			\item $a-a\lambda$ left over
		\end{enumerate}
		\pause
		\item Distribute this length equally across two intervals
		\begin{enumerate}
			\item Each have length $a(1-\lambda)/2$
			\item The leftmost one will be $[0,a(1-\lambda)/2]$
		\end{enumerate}
	\end{enumerate}

	Consider removing the middle $\lambda$ from $[0,1]$.
\end{frame}

\begin{frame}
	\frametitle{Constructing the Middle-$\lambda$ Cantor Set}

	\begin{center}
		\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=8.0cm,y=16.0cm]
		\clip(-0.15,-0.35) rectangle (1.05,-0.05);
		\draw [line width=.5mm] (0.,-0.1)-- (1.,-0.1);
		\draw [line width=.5mm] (-0.1,-0.08) node[anchor=north west] {$C_0$};
		\draw [line width=.5mm] (-0.02,-0.11) node[anchor=north west] {$0$};
		\draw [line width=.5mm] (0.9,-0.11) node[anchor=north west] {$l_0=1$};

		\pause
		\draw [line width=.5mm, color=cyan] (0.,-0.2)-- (1.,-0.2);
		\draw [line width=.5mm] (0.,-0.2)-- (0.25,-0.2);
		\draw [line width=.5mm] (0.75,-0.2)-- (1.,-0.2);
		\draw [line width=.5mm] (-0.1,-0.18) node[anchor=north west] {$C_1$};
		\draw [line width=.5mm] (0.22,-0.21) node[anchor=north west] {$l_1$};
		\draw [line width=.5mm] (-0.02,-0.21) node[anchor=north west] {$0$};
		\draw [line width=.5mm] (0.97,-0.21) node[anchor=north west] {$1$};

		\pause
		\draw [line width=.5mm, color=cyan] (0.,-0.3)-- (1.,-0.3);
		\draw [line width=.5mm] (0.,-0.3)-- (0.0625,-0.3);
		\draw [line width=.5mm] (0.1875,-0.3)-- (0.25,-0.3);
		\draw [line width=.5mm] (1.,-0.3)-- (0.9375,-0.3);
		\draw [line width=.5mm] (0.75,-0.3)-- (0.8125,-0.3);

		\draw [line width=.5mm] (-0.1,-0.28) node[anchor=north west] {$C_2$};
		\draw [line width=.5mm] (0.035,-0.31) node[anchor=north west] {$l_2$};
		\draw [line width=.5mm] (-0.02,-0.31) node[anchor=north west] {$0$};
		\draw [line width=.5mm] (0.97,-0.31) node[anchor=north west] {$1$};
		\end{tikzpicture}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Constructing the Middle-$\lambda$ Cantor Set}

	We iterate this \textit{ad infinitum}, and label what remains at the $n$th step $C_n$, and defining the middle-$\lambda$ Cantor set as $C=\cap_{n\in\NN} C_n$.

	\pause
	\vspace{2em}
	Suppose $l_n$ represents the length of each disjoint closed interval that comprises $C_n$.
	Thus $N_{l_n}(C)$ will be the number of those intervals.

\end{frame}

\begin{frame}
	\frametitle{Constructing the Middle-$\lambda$ Cantor Set}

	\begin{enumerate}
		\item We know that $l_1=(1-\lambda)/2$ and that $l_{n+1}=l_n(1-\lambda)/2$.
		\item We know that $C_1$ is the union of exactly two disjoint closed intervals $(N_{l_1}(C)=2)$ and that $N_{l_{n+1}}(C)=2n$.
		\pause
		\item These yield solutions $l_n=\paren{(1-\lambda)/2}^n$ and $N_{l_n}(C)=2^n$.
		\pause
		\item If we want to cover $C_n$ with sets of diameter $l_n$, we need exactly $2^n$ of them.
	\end{enumerate}
\end{frame}

\begin{frame}
	\frametitle{Finding the Box-Counting Dimension}

	To find the Box-counting dimension of $C$, we need to find
	\[
		\lim\limits_{\delta\rightarrow0}\frac{\ln N_\delta(C)}{-\ln\delta}.
	\]

	Fix $\delta >0$ and choose an $n$ such that $l_{n+1}\leq \delta < l_n$.

	\pause
	\vspace{1em}

	Thus, in order to cover $C$ we need
	\begin{itemize}
		\item no fewer than $2^n$ sets of diameter $\delta$
		\item no more than $2^{n+1}$ sets of diameter $\delta$
	\end{itemize}

	\pause

	\[
		2^n \leq N_\delta(C) \leq 2^{n+1}
	\]
\end{frame}

\begin{frame}
	\frametitle{Finding the Box-Counting Dimension}

	Through inequality magic, we get
	\[
		\frac{n\ln 2}{-\ln l_{n+1}} \leq \frac{\ln N_\delta(C)}{-\ln\delta} \leq \frac{(n+1)\ln 2}{-\ln l_n}.
	\]
	Substituting our formula for $l_n$ yields
	\[
		\frac{n\ln 2}{(n+1)\ln \paren{\frac{2}{1-\lambda}}} \leq \frac{\ln N_\delta(C)}{-\ln\delta} \leq \frac{(n+1)\ln 2}{n\ln \paren{\frac{2}{1-\lambda}}}.
	\]
\end{frame}

\begin{frame}
	\frametitle{The Box-Counting Dimension of $C$}

	If we let $\delta\rightarrow 0$, then $n\rightarrow\infty$ yielding:
	\[
		\frac{\ln 2}{\ln \paren{\frac{2}{1-\lambda}}} \leq \lim\limits_{\delta\rightarrow 0} \frac{\ln N_\delta(C)}{-\ln\delta} \leq \frac{\ln 2}{\ln \paren{\frac{2}{1-\lambda}}}.
	\]

	Thus, by Squeeze theorem
	\[
		\dim_B(C) = \frac{\ln 2}{\ln \paren{\frac{2}{1-\lambda}}}.
	\]
	\pause
\end{frame}

\begin{frame}
	\frametitle{Yet Another Cantor Set}
	
	\begin{enumerate}
		\item If $\lambda = 1/3$ then $\dim_B(C) = \ln 2 / \ln 3$.
	\end{enumerate}
	\pause
	\begin{center}
		What about the set of numbers in $[0,1]$ whose decimal expansions contain no 5's?
	\end{center}
	\pause
	\begin{center}
		\begin{tikzpicture}[line cap=round,line join=round,>=triangle 45,x=0.75cm,y=1.0cm]
		\clip(0.,1.5) rectangle (12.,2.5);
		\draw [line width=.5mm, color=green] (1.,2.)-- (11.,2.);
		\draw [line width=.5mm] (1.,2.)-- (6.,2.);
		\draw [line width=.5mm] (7.,2.)-- (11.,2.);
		\draw (2.,1.9)-- (2.,2.1);
		\draw (3.,2.1)-- (3.,1.9);
		\draw (4.,2.1)-- (4.,1.9);
		\draw (5.,2.1)-- (5.,1.9);
		\draw (6.,2.1)-- (6.,1.9);
		\draw (7.,2.1)-- (7.,1.9);
		\draw (8.,2.1)-- (8.,1.9);
		\draw (9.,2.1)-- (9.,1.9);
		\draw (10.,2.1)-- (10.,1.9);
		\draw (5.55,1.9) node[anchor=north west] {$0.5$};
		\draw (6.55,1.9) node[anchor=north west] {$0.6$};
		\draw (0.75,1.9) node[anchor=north west] {$0$};
		\draw (10.75,1.9) node[anchor=north west] {$1$};
		\end{tikzpicture}
	\end{center}
\end{frame}

\begin{frame}
	\frametitle{Yet Another Cantor Set}
	
	\begin{enumerate}
		\item Consider the formula for the middle-$\lambda$ Cantor set
		\[
			\dim_B(C) = \frac{\ln 2}{\ln \paren{\frac{2}{1-\lambda}}}.
		\]
		\pause
		\item More generally it will be
		\[
			\dim_B(C) = \frac{\ln (\text{number of subintervals})}{-\ln \paren{\text{length of each one}}}.
		\]
		\pause
		\item The box counting dimension of this new set will be
		\[
			\frac{\ln 9}{\ln 10}
		\]
	\end{enumerate}
\end{frame}

\begin{frame}[allowframebreaks]
\frametitle{References}
	\begin{thebibliography}{10}
		\bibitem{label}
			Kenneth Falconer
			\newblock Fractal Geometry, 3rd edition
			\newblock Wiley
	\end{thebibliography}
\end{frame}

\end{document}
